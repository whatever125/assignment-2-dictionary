global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60             ; 'exit' syscall number
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax                ; define counter with 0
    .counter:
        cmp byte [rdi + rax], 0 ; cmp if it is the end
        jz  .end                ; then return
        inc rax                 ; else - inc counter
        jmp .counter            ; loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi                ; save string address
    call string_length
    pop  rdi

    mov  rdx, rax           ; string length to rdx
    mov  rax, 1             ; 'write' syscall number
    mov  rsi, rdi           ; string address
    mov  rdi, 1             ; stdout descriptor
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error:
    push rdi                ; save string address
    call string_length
    pop  rdi

    mov  rdx, rax           ; string length to rdx
    mov  rax, 1             ; 'write' syscall number
    mov  rsi, rdi           ; string address
    mov  rdi, 2             ; stdout descriptor
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi                ; assign address to a symbol
    mov  rdx, 1             ; string length to rdx
    mov  rax, 1             ; 'write' syscall number
    mov  rsi, rsp           ; string address
    mov  rdi, 1             ; stdout descriptor
    syscall
    pop  rdi                ; restore rdi 
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov  rdi, 0xA           ; newline char
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov  rax, rdi               ; number
    mov  rcx, 10                ; radix
    push 0                      ; null-terminator
    .loop_save:
        xor  rdx, rdx           ; clear the remainder
        div  rcx                ; divide by redix
        add  rdx, '0'           ; digit to char
        push rdx                ; save the digit

        test rax, rax           ; number == 0?
        jnz  .loop_save         ; no - loop
    .loop_print:
        pop  rax                ; digit to rax
        test rax, rax           ; null-terminator?
        je   .done              ; yes - return
        mov  rdi, rax           ; no - prepare for print
        call print_char         ; print char
        jmp  .loop_print        ; loop
.done:
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi           ; number >= 0?
    jge  .print             ; yes - print as unsigned
    neg  rdi                ; no - number *= -1
    push rdi                ; save number
    mov  rdi, '-'           ; print minus sign
    call print_char
    pop  rdi                ; restore number
.print:
    call print_uint         ; print abs number
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor  rax, rax                   ; return value
    xor  rcx, rcx                   ; counter
    .loop:
        mov  dl, [rdi + rcx]        ; read char from 1st string
        cmp  dl, [rsi + rcx]        ; compare with char from 2nd string
        jne  .done                  ; not equal? - return 0 
        test dl, dl                 ; null-terminator?
        jz   .success               ; yes - return 1
        inc  rcx                    ; no - next position
        jmp  .loop                  ; loop
.success:
    mov  rax, 1
.done:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    sub rsp, 16         ; allocate 16 byte for a symbol

    xor rax, rax        ; 'read' syscall number
    xor rdi, rdi        ; stdin file descriptor
    mov rsi, rsp        ; rsi <- address of the buffer
    mov rdx, 1          ; rdx <- size of the buffer
    syscall             ; read symbol

    test rax, rax       ; no bytes read, null-terminator
    jz   .exit
    mov  rax, [rsp]     ; return read symbol
.exit:
    add rsp, 16         ; restore rsp
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push rbx                        ; save registers
    push r12
    push r13

    xor  rbx, rbx                   ; length of the word
    mov  r12, rdi                   ; save rdi - buffer address
    mov  r13, rsi                   ; save rsi - buffer size
    dec  r13                        ; buffer size decremented for null-terminator
    .loop1:
        call read_char              ; read new sym
        cmp  al, 0x20               ; sym == space?
        je   .loop1                 ; yes - loop
        cmp  al, 0x09               ; sym == tab?
        je   .loop1                 ; yes - loop
        cmp  al, 0x0A               ; sym == newline?
        je   .loop1                 ; yes - loop
        test al, al                 ; sym == null-terminator?
        je   .end                   ; yes - error
        jmp  .save                  ; word start
    .loop2:
        call read_char              ; read new sym
        test al, al                 ; sym == null-terminator?
        je   .end                   ; yes - end of word
        cmp  al,  0x20              ; sym == space?
        je   .end                   ; yes - end of word
        cmp  al,  0x09              ; sym == tab?
        je   .end                   ; yes - end of word
        cmp  al,  0x0A              ; sym == newline?
        je   .end                   ; yes - end of word
    .save:
        mov  byte [r12 + rbx], al   ; else - save sym
        inc  rbx                    ; length += 1
        cmp  rbx, r13               ; length >= buffer size?
        jl   .loop2                 ; else - loop
.too_long:
    xor rax, rax                    ; rax <- 0
    xor rdx, rdx                    ; rdx <- 0
    jmp .exit                       ; return
.end:
    mov byte [r12 + rbx], 0x0       ; add terminator
    mov rax, r12                    ; rax <- buffer address
    mov rdx, rbx                    ; rdx <- length
.exit:
    pop r13                         ; restore registers
    pop r12
    pop rbx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx                ; counter = 0
    xor rax, rax                ; final number = 0
    xor rsi, rsi                ; current digit = 0
    .loop:
        mov  sil, [rdi + rdx]   ; move current byte to rsi

        cmp  sil, '0'           ; less then '0'?
        jb   .done              ; yes - not a numeral, end of the number
        cmp  sil, '9'           ; more than '9'?
        ja   .done              ; yes - not a numeral, end of the number

        sub  sil, '0'           ; char to int
        imul rax, rax, 10       ; rax *= 10 - prepare for next digit
        add  rax, rsi           ; rax += rsi - save digit

        inc  rdx                ; inc counter
        jmp  .loop              ; loop
.done:
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov  sil, [rdi]         ; move current byte to rsi
    cmp  sil, '+'           ; signed number?
    je   .signed            ; yes
    cmp  sil, '-'           ; signed number?
    jne  parse_uint         ; no - call parse unsigned
.signed:                    ; yes - signed
    push rdi                ; save pointer
    inc  rdi                ; remove sign
    call parse_uint         ; read number
    pop  rdi                ; restore pointer
    test rdx, rdx           ; length == 0?
    jz   .done              ; yes - error, return
    inc  rdx                ; no - length += 1 (+/- sign)
    cmp  byte [rdi], '-'    ; below zero?
    jne  .done              ; no - done, return
    neg  rax                ; yes - multiply by -1
.done:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor  rax, rax                   ; counter
    .loop:
        mov  cl, [rdi + rax]        ; read current symbol
        mov  [rsi + rax], cl        ; copy symbol
        test cl, cl                 ; null-terminator?
        jz   .done                  ; yes - return
        inc  rax                    ; else - next symbol
        cmp  rax, rdx               ; rax < rdx?
        jl   .loop                  ; yes - loop
        xor  rax, rax               ; else - error, length = 0
.done:
    ret
