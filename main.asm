%include "dict.inc"
%include "lib.inc"

%include "words.inc"

section .bss
keyword: resb 256

section .rodata
READ_ERROR: db "ERROR: Key is too long", 0xA, 0
FIND_ERROR: db "ERROR: Key not found", 0xA, 0

section .text
global _start
_start:
    mov  rdi, keyword
    mov  rsi, 256
    call read_word

    test rax, rax
    jz   .read_fail

.read_success:
    mov  rdi, keyword
    mov  rsi, first_word
    call find_word

    test rax, rax
    jz   .find_fail

.find_success:
    mov  rdi, rax
    add  rdi, 8

    push rdi
    call string_length
    pop  rdi

    add  rdi, rax
    inc  rdi
    call print_string
	call print_newline

    xor  rdi, rdi
    jmp  exit

.read_fail:
    mov  rdi, READ_ERROR
    jmp  .fail

.find_fail:
    mov  rdi, FIND_ERROR
.fail:
    call print_error

    mov  rdi, 1
    jmp  exit
