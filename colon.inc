%define link 0

%macro colon 2
    %ifstr %1
        %ifid %2
            %2:
            dq link
            db %1, 0
            %define link %2
        %else
            %error "ERROR: Second parameter must be a label"
        %endif
    %else
        %error "ERROR: First parameter must be a string"
    %endif
%endmacro