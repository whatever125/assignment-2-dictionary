import subprocess

# Predefined input, expected stdout, and expected stderr
predefined_inputs = ["", "first_word", "third_word", "no_word", "toooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo_long_word"]
expected_stdouts = ["", "first word explanation", "third word explanation", "", ""]
expected_stderrs = ["ERROR: Key not found", "", "", "ERROR: Key not found", "ERROR: Key is too long"]

correct_runs = 0

for i in range(len(predefined_inputs)):
    user_input = predefined_inputs[i]
    expected_stdout = expected_stdouts[i]
    expected_stderr = expected_stderrs[i]

    # Run the program and capture stdout and stderr
    process = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    stdout, stderr = process.communicate(input=user_input)
    stdout, stderr = stdout.strip(), stderr.strip()

    print(f"Run {i + 1} - ", end="")

    # Check if stdout and stderr match the expected output
    if stdout == expected_stdout and stderr == expected_stderr:
        print(f"CORRECT")
        print(f"Input: {user_input}")
        print(f"Stdout: {stdout}")
        print(f"Stderr: {stderr}")
        correct_runs += 1
    else:
        print(f"INCORRECT")
        print(f"Input: {user_input}")

        if stdout != expected_stdout:
            print(f"Expected Stdout: {expected_stdout}")
            print(f"Actual Stdout: {stdout}")
        else:
            print(f"Stdout: {stdout}")

        if stderr != expected_stderr:
            print(f"Expected Stderr: {expected_stderr}")
            print(f"Actual Stderr: {stderr}")
        else:
            print(f"Stderr: {stderr}")

    print("-----")

print(f"Number of passed tests: {correct_runs}/{len(predefined_inputs)}")
